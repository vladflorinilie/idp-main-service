package ro.acs.idp.imageeditor.config;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.data.mongodb.core.index.IndexResolver;
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import ro.acs.idp.imageeditor.dao.User;

@Configuration
public class MongoConfiguration {

    private final MongoTemplate mongoTemplate;
    private final MongoConverter mongoConverter;

    MongoConfiguration(MongoTemplate mongoTemplate, MongoConverter mongoConverter) {
        this.mongoConverter = mongoConverter;
        this.mongoTemplate = mongoTemplate;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initIndicesAfterStartup() {
        IndexOperations indexOps = mongoTemplate.indexOps(User.class);
        MongoMappingContext mongoMappingContext = (MongoMappingContext) this.mongoConverter.getMappingContext();

        IndexResolver resolver = new MongoPersistentEntityIndexResolver(mongoMappingContext);
        resolver.resolveIndexFor(User.class).forEach(indexOps::ensureIndex);
    }
}
