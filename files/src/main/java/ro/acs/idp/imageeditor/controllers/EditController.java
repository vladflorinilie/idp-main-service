package ro.acs.idp.imageeditor.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import ro.acs.idp.imageeditor.dao.User;
import ro.acs.idp.imageeditor.dto.ImageDTO;
import ro.acs.idp.imageeditor.repositories.UserRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class EditController {

    @Value("${app.im.service.address}")
    private String imServiceAddress;
    @Value("${app.ud.files-folder}")
    private String udFolder;
    private UserRepository userRepository;

    EditController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/edit/{id}")
    public String getEdit(@PathVariable Integer id, Model model, HttpServletResponse response) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName);

        if (user.getImages() == null || user.getImages().stream().noneMatch(t -> t.getId().equals(id))) {
            response.sendRedirect("/home");
            return null;
        }

        ImageDTO image = user.getImages().stream().filter(t -> t.getId().equals(id)).findFirst().get();

        String[] loc = image.getLocation().split("\\.");
        model.addAttribute("original", user.getUsername() + "/" + loc[0] + "-original." + loc[1]);
        model.addAttribute("modified", user.getUsername() + "/" + loc[0] + "-modified." + loc[1]);
        model.addAttribute("id", id);

        return "edit";
    }

    @PostMapping("/edit")
    public String doEdit(@RequestParam("image") String image, @RequestParam("filter") String filter,
                         @RequestParam("id") Integer id, HttpServletResponse response) throws IOException {

        String fileLocation = udFolder + image;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForEntity(imServiceAddress + "?location=" + fileLocation + "&filter=" + filter,
                null);

        response.sendRedirect("/edit/" + id);

        return "edit";
    }
}
