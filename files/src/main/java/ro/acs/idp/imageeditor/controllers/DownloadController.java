package ro.acs.idp.imageeditor.controllers;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import ro.acs.idp.imageeditor.dao.User;
import ro.acs.idp.imageeditor.dto.ImageDTO;
import ro.acs.idp.imageeditor.repositories.UserRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class DownloadController {

    @Value("${app.ud.download.service.address}")
    private String udDownloadService;
    private UserRepository userRepository;

    DownloadController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/download/{id}")
    public String downloadImage(@PathVariable Integer id, HttpServletResponse response) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName);

        if (user.getImages() == null || user.getImages().stream().noneMatch(t -> t.getId().equals(id))) {
            response.sendRedirect("/home");
            return null;
        }

        ImageDTO image = user.getImages().stream().filter(t -> t.getId().equals(id)).findFirst().get();

        /* get file from download service */
        processRequest(user.getUsername(), image.getLocation(), response);

        return null;
    }

    public void processRequest(String username, String imageName, HttpServletResponse response) {
        RestTemplate restTemplate = new RestTemplate();

        response.setStatus(HttpStatus.OK.value());
        response.addHeader("Content-Type", "application/force-download");
        response.addHeader("Content-Disposition", "inline; filename=\"" + imageName + "\"");

        restTemplate.execute(
                udDownloadService + "/" + username + "/" + imageName,
                HttpMethod.GET,
                (ClientHttpRequest requestCallback) -> {},
                responseExtractor -> {
                    IOUtils.copy(responseExtractor.getBody(), response.getOutputStream());
                    return null;
                });
    }
}
