package ro.acs.idp.imageeditor.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.acs.idp.imageeditor.dao.User;
import ro.acs.idp.imageeditor.repositories.UserRepository;

import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class MainController {

    private Logger logger = Logger.getLogger(MainController.class.getName());
    private UserRepository userRepository;

    MainController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(path = {"/home", "/"})
    public String mainPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName);

        logger.log(Level.INFO, "Currently logged username: " + currentPrincipalName);

        model.addAttribute("images", user.getImages());

        return "home";
    }

}