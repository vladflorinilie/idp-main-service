package ro.acs.idp.imageeditor.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ImageDTO {
    private Integer id;
    private Date modifyDate;
    private String location;
}
