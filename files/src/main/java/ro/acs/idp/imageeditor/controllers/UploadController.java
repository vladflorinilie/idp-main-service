package ro.acs.idp.imageeditor.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import ro.acs.idp.imageeditor.dao.User;
import ro.acs.idp.imageeditor.dto.ImageDTO;
import ro.acs.idp.imageeditor.repositories.UserRepository;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class UploadController {

    @Value("${app.ud.upload.service.address}")
    private String udServiceAddress;
    private Logger logger = Logger.getLogger(UploadController.class.getName());
    private UserRepository userRepository;

    UploadController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/upload")
    public String formUploadImage() {
        return "upload";
    }

    @PostMapping("/upload")
    public String uploadImage(@RequestParam("file") MultipartFile file, Model model) {
        model.addAttribute("success", true);
        model.addAttribute("file", file.getOriginalFilename());

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName);

        /* use upload&download service to upload the image to the server */
        uploadWithService(file, user.getUsername());

        /* rest of processing */
        if (user.getImages() == null ||
                user.getImages().stream().noneMatch(t -> t.getLocation().equals(file.getOriginalFilename()))) {
            int lastUnused = 0;

            if (user.getImages() == null) {
                user.setImages(new LinkedList<>());
            } else if (!user.getImages().isEmpty()) {
                List<ImageDTO> images = user.getImages();
                lastUnused = images.get(images.size() - 1).getId() + 1;
            }

            Date currentDate = new Date();
            ImageDTO image = new ImageDTO();
            image.setLocation(file.getOriginalFilename());
            image.setModifyDate(currentDate);
            image.setId(lastUnused);

            user.getImages().add(image);

            userRepository.save(user);
        }

        return "upload";
    }

    public void uploadWithService(MultipartFile file, String username) {
        logger.log(Level.INFO, "Received file for upload: " + file.getOriginalFilename());

        Resource fileResource = file.getResource();

        LinkedMultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("file", fileResource);
        parts.add("username", username);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(parts, httpHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(udServiceAddress, httpEntity, null);
    }

}
