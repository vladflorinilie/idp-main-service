package ro.acs.idp.imageeditor.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import ro.acs.idp.imageeditor.dto.ImageDTO;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Document(collection = "user")
@Getter
@Setter
public class User {
    @Id
    private String id;

    @NotBlank
    @Indexed(unique = true)
    private String username;

    @NotBlank
    private String password;

    private List<ImageDTO> images;
}
