package ro.acs.idp.imageeditor.controllers;

import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.acs.idp.imageeditor.dto.UserDTO;
import ro.acs.idp.imageeditor.dao.User;
import ro.acs.idp.imageeditor.repositories.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static ro.acs.idp.imageeditor.config.security.SecurityConstants.TOKEN_PREFIX;

@Controller
public class AuthController {

    private UserRepository userRepository;

    AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/register")
    public String getCreateAccount() {
        return "register";
    }

    @PostMapping(path = "/register", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String postCreateAccount(UserDTO userDTO, Model model) {
        if (userRepository.findByUsername(userDTO.getUsername()) != null) {
            /* signal error */
            model.addAttribute("registerError", true);

            return "register";
        }

        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
        userRepository.save(user);

        return "login";
    }

    @GetMapping("/login")
    public String accountLogin(@RequestParam(required = false) String error, Model model) {
        if (error != null) {
            model.addAttribute("loginError", true);
        }

        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletResponse response) {
        Cookie cookie = new Cookie(TOKEN_PREFIX, null);
        cookie.setMaxAge(0);
        cookie.setHttpOnly(true);

        response.addCookie(cookie);

        return "login";
    }

}
