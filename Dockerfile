FROM maven:3.6.3-jdk-8

ADD files /opt/imageeditor

WORKDIR /opt/imageeditor

RUN mvn install -DskipTests

EXPOSE 8080

WORKDIR /opt/imageeditor/target

ENTRYPOINT ["java", "-jar", "image-editor-0.0.1-SNAPSHOT.jar"]